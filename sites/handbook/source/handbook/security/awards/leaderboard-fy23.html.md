---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 3500 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 2060 |
| [@leipert](https://gitlab.com/leipert) | 3 | 2000 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 1420 |
| [@dblessing](https://gitlab.com/dblessing) | 5 | 1220 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 6 | 1120 |
| [@djadmin](https://gitlab.com/djadmin) | 7 | 1120 |
| [@Alexand](https://gitlab.com/Alexand) | 8 | 1000 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 9 | 900 |
| [@brodock](https://gitlab.com/brodock) | 10 | 800 |
| [@.luke](https://gitlab.com/.luke) | 11 | 700 |
| [@toon](https://gitlab.com/toon) | 12 | 700 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 13 | 660 |
| [@kassio](https://gitlab.com/kassio) | 14 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 15 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 16 | 600 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 17 | 540 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 18 | 530 |
| [@kerrizor](https://gitlab.com/kerrizor) | 19 | 520 |
| [@ifarkas](https://gitlab.com/ifarkas) | 20 | 520 |
| [@ratchade](https://gitlab.com/ratchade) | 21 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 22 | 500 |
| [@jlear](https://gitlab.com/jlear) | 23 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 24 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 25 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 26 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 27 | 500 |
| [@mc_rocha](https://gitlab.com/mc_rocha) | 28 | 500 |
| [@toupeira](https://gitlab.com/toupeira) | 29 | 480 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 30 | 460 |
| [@garyh](https://gitlab.com/garyh) | 31 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 32 | 440 |
| [@manojmj](https://gitlab.com/manojmj) | 33 | 440 |
| [@stanhu](https://gitlab.com/stanhu) | 34 | 400 |
| [@xanf](https://gitlab.com/xanf) | 35 | 400 |
| [@markrian](https://gitlab.com/markrian) | 36 | 400 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 37 | 350 |
| [@bmarjanovic](https://gitlab.com/bmarjanovic) | 38 | 320 |
| [@rcobb](https://gitlab.com/rcobb) | 39 | 300 |
| [@dzubova](https://gitlab.com/dzubova) | 40 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 41 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 42 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 43 | 300 |
| [@tle_gitlab](https://gitlab.com/tle_gitlab) | 44 | 300 |
| [@ahegyi](https://gitlab.com/ahegyi) | 45 | 260 |
| [@vshushlin](https://gitlab.com/vshushlin) | 46 | 240 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 47 | 230 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 48 | 230 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 49 | 220 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 50 | 210 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 51 | 200 |
| [@pshutsin](https://gitlab.com/pshutsin) | 52 | 200 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 53 | 200 |
| [@pursultani](https://gitlab.com/pursultani) | 54 | 180 |
| [@10io](https://gitlab.com/10io) | 55 | 170 |
| [@mwoolf](https://gitlab.com/mwoolf) | 56 | 160 |
| [@egrieff](https://gitlab.com/egrieff) | 57 | 160 |
| [@tachyons-gitlab](https://gitlab.com/tachyons-gitlab) | 58 | 150 |
| [@rmarshall](https://gitlab.com/rmarshall) | 59 | 140 |
| [@avielle](https://gitlab.com/avielle) | 60 | 140 |
| [@mbobin](https://gitlab.com/mbobin) | 61 | 130 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 62 | 130 |
| [@alexpooley](https://gitlab.com/alexpooley) | 63 | 130 |
| [@lauraX](https://gitlab.com/lauraX) | 64 | 120 |
| [@balasankarc](https://gitlab.com/balasankarc) | 65 | 110 |
| [@ck3g](https://gitlab.com/ck3g) | 66 | 110 |
| [@mattkasa](https://gitlab.com/mattkasa) | 67 | 100 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 68 | 100 |
| [@drew](https://gitlab.com/drew) | 69 | 90 |
| [@dmakovey](https://gitlab.com/dmakovey) | 70 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 71 | 80 |
| [@splattael](https://gitlab.com/splattael) | 72 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 73 | 80 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 74 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 75 | 60 |
| [@minac](https://gitlab.com/minac) | 76 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 77 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 78 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 79 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 80 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 81 | 60 |
| [@cngo](https://gitlab.com/cngo) | 82 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 83 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 84 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 85 | 60 |
| [@abdwdd](https://gitlab.com/abdwdd) | 86 | 60 |
| [@ahmed.hemdan](https://gitlab.com/ahmed.hemdan) | 87 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 88 | 60 |
| [@dmishunov](https://gitlab.com/dmishunov) | 89 | 60 |
| [@tianwenchen](https://gitlab.com/tianwenchen) | 90 | 60 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 91 | 60 |
| [@khanmansoor](https://gitlab.com/khanmansoor) | 92 | 60 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 93 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 94 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 95 | 40 |
| [@mhenriksen](https://gitlab.com/mhenriksen) | 96 | 40 |
| [@morefice](https://gitlab.com/morefice) | 97 | 40 |
| [@jessieay](https://gitlab.com/jessieay) | 98 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 99 | 30 |
| [@subashis](https://gitlab.com/subashis) | 100 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 101 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 102 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 103 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 104 | 30 |
| [@acroitor](https://gitlab.com/acroitor) | 105 | 30 |
| [@carlad-gl](https://gitlab.com/carlad-gl) | 106 | 30 |
| [@nmilojevic1](https://gitlab.com/nmilojevic1) | 107 | 30 |
| [@dgruzd](https://gitlab.com/dgruzd) | 108 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 109 | 30 |
| [@cablett](https://gitlab.com/cablett) | 110 | 30 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 111 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 112 | 20 |
| [@terrichu](https://gitlab.com/terrichu) | 113 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 800 |
| [@ashmckenzie](https://gitlab.com/ashmckenzie) | 2 | 600 |
| [@greg](https://gitlab.com/greg) | 3 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 440 |
| [@f_santos](https://gitlab.com/f_santos) | 5 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 6 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 7 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 8 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 9 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 10 | 30 |
| [@fneill](https://gitlab.com/fneill) | 11 | 30 |
| [@seanarnold](https://gitlab.com/seanarnold) | 12 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 2 | 400 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 3 | 400 |
| [@cguitarte](https://gitlab.com/cguitarte) | 4 | 300 |
| [@ankit.panchal](https://gitlab.com/ankit.panchal) | 5 | 80 |
| [@vburton](https://gitlab.com/vburton) | 6 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@mehulsharma](https://gitlab.com/mehulsharma) | 2 | 500 |
| [@feistel](https://gitlab.com/feistel) | 3 | 400 |
| [@tnir](https://gitlab.com/tnir) | 4 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 5 | 300 |
| [@kyrie.31415926535](https://gitlab.com/kyrie.31415926535) | 6 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 7 | 200 |
| [@zined](https://gitlab.com/zined) | 8 | 200 |
| [@trakos](https://gitlab.com/trakos) | 9 | 200 |

## FY23-Q3

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@dblessing](https://gitlab.com/dblessing) | 1 | 1100 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 550 |
| [@Alexand](https://gitlab.com/Alexand) | 3 | 400 |
| [@manojmj](https://gitlab.com/manojmj) | 4 | 400 |
| [@bmarjanovic](https://gitlab.com/bmarjanovic) | 5 | 320 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 6 | 180 |
| [@pursultani](https://gitlab.com/pursultani) | 7 | 180 |
| [@tachyons-gitlab](https://gitlab.com/tachyons-gitlab) | 8 | 150 |
| [@djadmin](https://gitlab.com/djadmin) | 9 | 140 |
| [@sabrams](https://gitlab.com/sabrams) | 10 | 120 |
| [@ck3g](https://gitlab.com/ck3g) | 11 | 110 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 12 | 90 |
| [@splattael](https://gitlab.com/splattael) | 13 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 14 | 80 |
| [@balasankarc](https://gitlab.com/balasankarc) | 15 | 70 |
| [@ahmed.hemdan](https://gitlab.com/ahmed.hemdan) | 16 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 17 | 60 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 18 | 60 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 19 | 60 |
| [@pshutsin](https://gitlab.com/pshutsin) | 20 | 60 |
| [@rmarshall](https://gitlab.com/rmarshall) | 21 | 60 |
| [@dmishunov](https://gitlab.com/dmishunov) | 22 | 60 |
| [@.luke](https://gitlab.com/.luke) | 23 | 60 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 24 | 60 |
| [@tianwenchen](https://gitlab.com/tianwenchen) | 25 | 60 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 26 | 60 |
| [@khanmansoor](https://gitlab.com/khanmansoor) | 27 | 60 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 28 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 29 | 50 |
| [@morefice](https://gitlab.com/morefice) | 30 | 40 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 31 | 40 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 32 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 33 | 40 |
| [@jessieay](https://gitlab.com/jessieay) | 34 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 35 | 40 |
| [@egrieff](https://gitlab.com/egrieff) | 36 | 40 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 37 | 30 |
| [@mwoolf](https://gitlab.com/mwoolf) | 38 | 30 |
| [@carlad-gl](https://gitlab.com/carlad-gl) | 39 | 30 |
| [@nmilojevic1](https://gitlab.com/nmilojevic1) | 40 | 30 |
| [@dgruzd](https://gitlab.com/dgruzd) | 41 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 42 | 30 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 43 | 30 |
| [@cablett](https://gitlab.com/cablett) | 44 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 300 |
| [@seanarnold](https://gitlab.com/seanarnold) | 2 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@cguitarte](https://gitlab.com/cguitarte) | 1 | 300 |
| [@ankit.panchal](https://gitlab.com/ankit.panchal) | 2 | 80 |

### Community

Category is empty

## FY23-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 2300 |
| [@sabrams](https://gitlab.com/sabrams) | 2 | 1300 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 3 | 900 |
| [@toon](https://gitlab.com/toon) | 4 | 700 |
| [@Alexand](https://gitlab.com/Alexand) | 5 | 600 |
| [@mc_rocha](https://gitlab.com/mc_rocha) | 6 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 7 | 480 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 8 | 430 |
| [@ifarkas](https://gitlab.com/ifarkas) | 9 | 420 |
| [@markrian](https://gitlab.com/markrian) | 10 | 400 |
| [@dzubova](https://gitlab.com/dzubova) | 11 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 12 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 13 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 14 | 300 |
| [@tle_gitlab](https://gitlab.com/tle_gitlab) | 15 | 300 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 16 | 260 |
| [@ahegyi](https://gitlab.com/ahegyi) | 17 | 260 |
| [@toupeira](https://gitlab.com/toupeira) | 18 | 240 |
| [@vshushlin](https://gitlab.com/vshushlin) | 19 | 240 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 20 | 220 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 21 | 200 |
| [@brodock](https://gitlab.com/brodock) | 22 | 200 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 23 | 200 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 24 | 180 |
| [@avielle](https://gitlab.com/avielle) | 25 | 140 |
| [@drew](https://gitlab.com/drew) | 26 | 90 |
| [@kerrizor](https://gitlab.com/kerrizor) | 27 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 28 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 29 | 80 |
| [@.luke](https://gitlab.com/.luke) | 30 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 31 | 70 |
| [@10io](https://gitlab.com/10io) | 32 | 70 |
| [@egrieff](https://gitlab.com/egrieff) | 33 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 34 | 60 |
| [@tkuah](https://gitlab.com/tkuah) | 35 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 36 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 37 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 38 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 39 | 60 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 40 | 60 |
| [@abdwdd](https://gitlab.com/abdwdd) | 41 | 60 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 42 | 40 |
| [@manojmj](https://gitlab.com/manojmj) | 43 | 40 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 44 | 40 |
| [@mhenriksen](https://gitlab.com/mhenriksen) | 45 | 40 |
| [@alexpooley](https://gitlab.com/alexpooley) | 46 | 30 |
| [@mbobin](https://gitlab.com/mbobin) | 47 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 48 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 49 | 30 |
| [@acroitor](https://gitlab.com/acroitor) | 50 | 30 |
| [@terrichu](https://gitlab.com/terrichu) | 51 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@ashmckenzie](https://gitlab.com/ashmckenzie) | 1 | 600 |
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 2 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 3 | 40 |
| [@fneill](https://gitlab.com/fneill) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 2 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@mehulsharma](https://gitlab.com/mehulsharma) | 1 | 500 |
| [@tnir](https://gitlab.com/tnir) | 2 | 400 |
| [@kyrie.31415926535](https://gitlab.com/kyrie.31415926535) | 3 | 300 |
| [@trakos](https://gitlab.com/trakos) | 4 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1450 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 1200 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 900 |
| [@kassio](https://gitlab.com/kassio) | 5 | 600 |
| [@brodock](https://gitlab.com/brodock) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 18 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 19 | 460 |
| [@garyh](https://gitlab.com/garyh) | 20 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 21 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 22 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 23 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 24 | 400 |
| [@xanf](https://gitlab.com/xanf) | 25 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 26 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 27 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 28 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 29 | 150 |
| [@pshutsin](https://gitlab.com/pshutsin) | 30 | 140 |
| [@10io](https://gitlab.com/10io) | 31 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 32 | 100 |
| [@alexpooley](https://gitlab.com/alexpooley) | 33 | 100 |
| [@ifarkas](https://gitlab.com/ifarkas) | 34 | 100 |
| [@dmakovey](https://gitlab.com/dmakovey) | 35 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 36 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 37 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 38 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 39 | 60 |
| [@minac](https://gitlab.com/minac) | 40 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 41 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 42 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 43 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 44 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 45 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 46 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 47 | 60 |
| [@cngo](https://gitlab.com/cngo) | 48 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 49 | 50 |
| [@mbobin](https://gitlab.com/mbobin) | 50 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 51 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 52 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 53 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 54 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 55 | 30 |
| [@subashis](https://gitlab.com/subashis) | 56 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 57 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 58 | 30 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 59 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 60 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 61 | 30 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 62 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 63 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 64 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 65 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 66 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 2 | 400 |
| [@f_santos](https://gitlab.com/f_santos) | 3 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 4 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 5 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 6 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 7 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 8 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 1 | 400 |
| [@vburton](https://gitlab.com/vburton) | 2 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |


